# Wellfound's microblog assessment!

## About
This is a clone of the Wellfound assignment found [here](https://gitlab.com/wellfound-assessment/microblog).

I have update the home page of the Flask app to display all of the rows in the blog posts table instead of just the static single row as it was initially.

You can see a screenshot of the application home page below:

![screenshot](https://gitlab.com/zeserj/microblog/-/raw/e6341a1ec929c89555d9dc58fe5e33e342b24736/screenshot.png?inline=false)

